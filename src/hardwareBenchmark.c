#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define ever (;;)

void errorEndExecution(char* message)
{
	fprintf(stderr, "hardwareBenchmark : %s : %s\n", message, strerror(errno));
	exit(EXIT_FAILURE);
}

void ramTest()
{
	int exitCode;
	
	exitCode = system("ramTest");
	if (exitCode == -1) {
		errorEndExecution("system error: child Prozess could not be created");
	} 
	if (WIFEXITED(exitCode) && ((WEXITSTATUS(exitCode) == 126) || (WEXITSTATUS(exitCode) == 127))) {
		errorEndExecution("system error: ramTest can't be started");
	} else if (exitCode != 0) {
		printf("%s", strerror(WEXITSTATUS(exitCode)));
		errorEndExecution("ramTest Error");
	}
}
	
void cpuTest()
{
	int exitCode;
	
	exitCode = system("cpuTest");
	if (exitCode == -1) {
		errorEndExecution("system error: child Prozess could not be created");
	} 
	if (WIFEXITED(exitCode) && ((WEXITSTATUS(exitCode) == 126) || (WEXITSTATUS(exitCode) == 127))) {
		errorEndExecution("system error: cpuTest can't be started");
	} else if (exitCode != 0) {
		printf("%s", strerror(WEXITSTATUS(exitCode)));
		errorEndExecution("cpuTest Error");
	}
	
}

void showPartitions()
{
	FILE* partitions;
	FILE* vendorFile;
	FILE* modelFile;
	char line[1024];
	char device[64];
	char vendor[128];
	char model[128];
	int minor, paraRead;
	
	
	partitions = fopen("/proc/partitions", "r");
	if (partitions == NULL) errorEndExecution("fopen error");

	// over read the first 2 lines
	fgets(line, 1024, partitions);
	printf("\n%s", line);
	fgets(line, 1024, partitions);
	printf("%s", line);
	
	while(fgets(line, 1024, partitions))
	{
		
		//line[strlen(line)-1] = ' ';
		line[strcspn(line, "\n")] = ' ';
		
		paraRead = sscanf(line, " %*i %i %*i %s", &minor, device);
		if(paraRead == 2)
		{
			if ((minor % 16) == 0 )
			{
				strcat(line, " ");
				
				sprintf(vendor, "/sys/block/%s/device/vendor", device);
				vendorFile = fopen(vendor, "r");
				if(vendorFile != NULL)
				{
					if(fgets(vendor, 128, vendorFile) != NULL)
					{
						vendor[strcspn(vendor, "\n")] = ' ';
						strcat(line, vendor);
					}
					fclose(vendorFile);
				}
				sprintf(model, "/sys/block/%s/device/model", device);
				modelFile = fopen(model, "r");
				if(modelFile != NULL)
				{
					if(fgets(model, 128, modelFile) != NULL)
					{
						model[strcspn(model, "\n")] = ' ';
						strcat(line, model);
					}
					fclose(modelFile);
				}			
			}
			else // partition
			{
				strcat(line, "Part");
			}
		}
		printf("%s\n", line);
	}
	fclose(partitions);
}

void diskTest()
{
	char* blockDevice;
	char blkDevTestProgram[80] = "blockDeviceTest /dev/";
	int exitCode;
	char c;
		
	printf("\nBlock Device Test\n");
	
	showPartitions();
	
	printf("\nPlease enter a device identifier, like 'sda' for example\n");

	exitCode = scanf(" %ms", &blockDevice);
	if (exitCode != 1) errorEndExecution("scanf error");

	while ( (c = getchar()) != '\n' && c != EOF ){}

	strcat(blkDevTestProgram, blockDevice);

	printf("%s\n", blkDevTestProgram);
	exitCode = system(blkDevTestProgram);
	if (exitCode == -1) {
		errorEndExecution("system error: child Prozess could not be created ");
	}
	if (WIFEXITED(exitCode) && ((WEXITSTATUS(exitCode) == 126) || (WEXITSTATUS(exitCode) == 127))) {
		errorEndExecution("system error: blockDeviceTest can't be started' ");
	} else if (exitCode != 0) errorEndExecution("blockDeviceTest Error");


	free(blockDevice);
}

int main(int argc, char **argv)
{
	int input, c;

	
	printf("\nHardware Benchmark der DHGE\n");
	printf("===========================\n");

	for ever
	{
		printf("\n1 ) RAM Test\n");
		printf("2 ) CPU Test\n");
		printf("3 ) Festplatten Test\n\n");
		
		printf("0 ) exit\n\n");
		
		
		input = getchar();
		while ( (c = getchar()) != '\n' && c != EOF ){}
		
		switch(input)
		{
			case '0' :
				exit(0);
			case '1' : 
				ramTest();
				break;
			case '2' :
				cpuTest();
				break;
			case '3' :
				diskTest();
				break;
			default:
				printf("invalid input\n\n");
				break;
		}
	}

	return 0;
}
