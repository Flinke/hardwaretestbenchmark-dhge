#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

//Linear Congruential Generator: z = (a * z + b) & c
#define LIN_CON_MUL 1103515245
#define LIN_CON_ADD 12345

// 2^26
#define TEST_DATA_LENGTH_RAND  67108864
#define LIN_CON_MOD_RAND 0x03FFFFFF

// 2^30;
#define TEST_DATA_LENGTH  1073741824
#define LIN_CON_MOD 0x3FFFFFFF

// 2^7
//#define TEST_DATA_LENGTH  128

void errorEndExecution(char* message)
{
	fprintf(stderr, "blockDeviceTest : %s : %s\n", message, strerror(errno));
	exit(EXIT_FAILURE);
}

long double calculateDataPerSec(ssize_t rwSize, struct timespec timeEnd, struct timespec timeBegin, long dryRunTimeNsec)
{
	unsigned long secToNsec = (timeEnd.tv_sec - timeBegin.tv_sec) * 1e9;
	long double timeSec = (timeEnd.tv_nsec + secToNsec) - timeBegin.tv_nsec - dryRunTimeNsec;
	timeSec /= 1e9; // ns to s
	printf("Time: %Lf s\n", timeSec);
	
	return rwSize / timeSec;
}

off_t setOffset(int fd, off_t offset)
{
	off_t offsetNew;
	offsetNew = lseek(fd, offset, SEEK_SET);
	if (offsetNew == -1) errorEndExecution("offset error");
	if (offset != offsetNew)
	{
		printf("offset not at %li, instead at %li", offset, offsetNew);
		exit(EXIT_FAILURE);
	}
	
	return offsetNew;
}

void dropCaches()
{
	printf("dropping caches\n");
	sync();
	int error = system("echo 1 > /proc/sys/vm/drop_caches");
	if (error == -1) {
		errorEndExecution("system error: child Prozess could not be created ");
	}
	if (WIFEXITED(error) && ((WEXITSTATUS(error) == 126) || (WEXITSTATUS(error) == 127))) {
		errorEndExecution("system error: blockDeviceTest can't be started' ");
	} else if (error != 0) errorEndExecution("system error");
	
	sleep(1);	
}

int main(int argc, char **argv)
{
	int error;
	unsigned int i;
	unsigned long randCycles;
	int fd;
	ssize_t sizeRead, sizeWrite;
	long sizeReadTotal, sizeWriteTotal;
	off_t offset;
	char* buf;
	struct timespec timeBegin, timeEnd;
	long double bytePerSec;
	unsigned int randNum;
	
	
	if (argc != 2) {
		printf("input error");
		//todo specify input error
	}
	
	buf = calloc(TEST_DATA_LENGTH, sizeof(char));
	if (buf == NULL) errorEndExecution("calloc error");
	
	// drop caches
	dropCaches();
	
	fd = open(argv[1], O_RDWR | O_DSYNC);
	if (fd == -1) errorEndExecution("open error");

	// ####### READ SEQ
	printf("\nSequential reading:\n");
	clock_gettime(CLOCK_MONOTONIC, &timeBegin);
	sizeRead = read(fd, buf, TEST_DATA_LENGTH);
	if (sizeRead == -1) errorEndExecution("read error");	
	clock_gettime(CLOCK_MONOTONIC, &timeEnd);
	

	bytePerSec = calculateDataPerSec(sizeRead, timeEnd, timeBegin, 0);
	bytePerSec /= 1048576; // Byte to MiB
	printf("Size read: %ld MiB\n", (sizeRead / (1048576)));
	printf("Read: %.3Lf MiB/s\n", bytePerSec);
	
	// set offset 0
	offset = setOffset(fd, 0);
	if (offset != 0) {
		printf("offset not at 0 %li", offset);
		exit(EXIT_FAILURE);
	}

	// ####### WRITE SEQ
	printf("\nSequential writing:\n");
	clock_gettime(CLOCK_MONOTONIC, &timeBegin);
	sizeWrite = write(fd, buf, TEST_DATA_LENGTH);
	if (sizeWrite == -1) errorEndExecution("write error");
	clock_gettime(CLOCK_MONOTONIC, &timeEnd);
	
	if (sizeWrite != sizeRead) printf("size read does not match size written");
	
	bytePerSec = calculateDataPerSec(sizeWrite, timeEnd, timeBegin, 0);
	bytePerSec /= 1048576;
	printf("Size written: %ld MiB\n", (sizeWrite / (1048576)));
	printf("Write: %.3Lf MiB/s\n", bytePerSec);
	
	// ####### Random Number Generator Dry Run
	randNum = 1;
	clock_gettime(CLOCK_MONOTONIC, &timeBegin);
	for(i=0; i < 32768; ++i)
	{
		// random generator 27bit for 128MiB
		randNum = (LIN_CON_MUL * randNum + LIN_CON_ADD) & LIN_CON_MOD_RAND;

		setOffset(fd, randNum);
		sizeReadTotal += sizeRead;
	}
	clock_gettime(CLOCK_MONOTONIC, &timeEnd);
	
	long secToNsec = (timeEnd.tv_sec - timeBegin.tv_sec) * 1e9;
	long double timeDryRunSec = (timeEnd.tv_nsec + secToNsec) - timeBegin.tv_nsec;
	timeDryRunSec /= 1e9; // ns to s
	//printf("Time Random Number Generator dry run: %Lf s\n", timeDryRunSec);

	// ######## READ RAND
	printf("\nRandom reading 4K blocks:\n");
	// drop caches
	dropCaches();

	sizeReadTotal = 0;
	randNum = 1;
	randCycles = TEST_DATA_LENGTH_RAND >>12; // because of writing 4K blocks
	//printf("rand cycles: %li\n", randCycles);
	
	// set offset 0
	offset = setOffset(fd, 0);
	if (offset != 0) {
		printf("offset not at 0 %li", offset);
		exit(EXIT_FAILURE);
	}
	
	clock_gettime(CLOCK_MONOTONIC, &timeBegin);
	for(i=0; i < randCycles; ++i)
	{
		// random generator 27bit for 128MiB
		randNum = (LIN_CON_MUL * randNum + LIN_CON_ADD) & LIN_CON_MOD_RAND;

		setOffset(fd, randNum);
		sizeRead = read(fd, buf, 4096);
		if (sizeRead == -1) errorEndExecution("read error");	
		sizeReadTotal += sizeRead;
	}
	clock_gettime(CLOCK_MONOTONIC, &timeEnd);

	bytePerSec = calculateDataPerSec(sizeReadTotal, timeEnd, timeBegin, 0);
	bytePerSec /= 1048576;
	printf("Size read random: %ld MiB\n", (sizeReadTotal >>20));
	printf("Read random: %.3Lf MiB/s\n", bytePerSec);
	
	// ######## WRITE RAND
	printf("\nRandom writing 4K blocks:\n");
	sizeWriteTotal = 0;
	randNum = 1;
	
	// set offset 0
	offset = setOffset(fd, 0);
	if (offset != 0) {
		printf("offset not at 0 %li", offset);
		exit(EXIT_FAILURE);
	}
	sizeRead = read(fd, buf, TEST_DATA_LENGTH_RAND);
	if (sizeRead == -1) errorEndExecution("read error");	
	if (sizeRead != TEST_DATA_LENGTH_RAND) printf("read not full TEST_DATA_LENGTH");

	randCycles = sizeRead >>12;

	clock_gettime(CLOCK_MONOTONIC, &timeBegin);
	for(i=0; i < randCycles; ++i)
	{
		// random generator 27bit for 128MiB
		randNum = (LIN_CON_MUL * randNum + LIN_CON_ADD) & LIN_CON_MOD_RAND;

		setOffset(fd, randNum);
		sizeWrite = write(fd, (buf + randNum), 4096);
		if (sizeWrite == -1) errorEndExecution("write error");
		sizeWriteTotal += sizeWrite;
	}
	clock_gettime(CLOCK_MONOTONIC, &timeEnd);

	bytePerSec = calculateDataPerSec(sizeWriteTotal, timeEnd, timeBegin, 0);
	bytePerSec /= 1048576;
	printf("Size written random: %ld MiB\n", (sizeWriteTotal >>20));
	printf("Write random: %.3Lf MiB/s\n", bytePerSec);

	// close fd
	error = close(fd);
	if (error == -1) errorEndExecution("close error");

	return EXIT_SUCCESS;
}
