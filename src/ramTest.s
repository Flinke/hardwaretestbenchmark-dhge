
.intel_syntax noprefix

.text

.globl ramTestSequentialDryRun, ramWriteSequential64, ramReadSequ64, ramWriteSequential32, ramReadSequ32, ramWriteSequential16, ramReadSequ16, ramWriteSequential8, ramReadSequ8, ramDryRunRandom, ramWriteRandom64, ramReadRandom64, ramWriteRandom32, ramReadRandom32, ramWriteRandom16, ramReadRandom16, ramWriteRandom8, ramReadRandom8

											// ramWriteSequential64
ramWriteSequential64:
	
	MOV rbx, 0xAAAAAAAAAAAAAAAA
	MOV rax, 8

	
beginWritingDATA64:

	MOV qword ptr [rdi], rbx
	ADD rdi, rax
	
	DEC rsi
	JNZ beginWritingDATA64
	
	ret
	
											// ramReadSequential64
ramReadSequ64:

	MOV rax, 8

beginReadData64:

	MOV rbx, qword ptr [rdi]
	ADD rdi, rax
	
	DEC rsi
	JNZ beginReadData64
	
	ret

											// ramWriteSequential32
ramWriteSequential32:
	
	MOV ebx, 0xAAAAAAAA
	MOV rax, 4

	
beginWritingDATA32:

	MOV dword ptr [rdi], ebx
	ADD rdi, rax
	
	DEC rsi
	JNZ beginWritingDATA32
	
	ret
	
											// ramReadSequential32
ramReadSequ32:

	MOV rax, 4

beginReadData32:

	MOV ebx, dword ptr [rdi]
	ADD rdi, rax
	
	DEC rsi
	JNZ beginReadData32
	
	ret
	

											// ramWriteSequential16
ramWriteSequential16:
	
	MOV bx, 0xAAAA
	MOV rax, 2

	
beginWritingDATA16:

	MOV word ptr [rdi], bx
	ADD rdi, rax
	
	DEC rsi
	JNZ beginWritingDATA16
	
	ret
	
											// ramReadSequential16
ramReadSequ16:

	MOV rax, 2

beginReadData16:

	MOV bx, word ptr [rdi]
	ADD rdi, rax
	
	DEC rsi
	JNZ beginReadData16
	
	ret
			
											// ramWriteSequential8
ramWriteSequential8:
	
	MOV bl, 0xAA
	MOV rax, 1

	
beginWritingDATA8:

	MOV byte ptr [rdi], bl
	ADD rdi, rax
	
	DEC rsi
	JNZ beginWritingDATA8
	
	ret
			
											// ramReadSequ8
ramReadSequ8:
	
	MOV bl, 0xAA
	MOV rax, 1

	
beginReadingDATA8:

	MOV bl, byte ptr [rdi]
	ADD rdi, rax
	
	DEC rsi
	JNZ beginReadingDATA8
	
	ret
	
											// dryRunSequential
ramTestSequentialDryRun:
	
	MOV rbx, 0xAAAAAAAAAAAAAAAA
	MOV rax, 8
	
beginWritingDATADryRun:

	ADD rdi, rax
	
	DEC rsi
	JNZ beginWritingDATADryRun
	
	ret
	
											// dryRunRandom
ramDryRunRandom:

	MOV rbx, 0xAAAAAAAAAAAAAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx  must be 2^x
	// a = r9
	// b = r10
	// z = rax
	
beginDryRunRandom:
	
	
	MUL r9
	ADD rax, r10
	AND rax, rcx
	
	SHL rax, 3
	ADD rdi, rax
	
	DEC rsi
	JNZ beginDryRunRandom
	
	ret
	
											// ramWriteRandom64
ramWriteRandom64:

	MOV rbx, 0xAAAAAAAAAAAAAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginWritingRandom64:

	MUL r9
	ADD rax, r10
	AND rax, rcx
	
	MOV qword ptr [rdi + (rax * 8)], rbx
		
	DEC rsi
	JNZ beginWritingRandom64
	
	ret
	
											// ramReadRandom64
ramReadRandom64:

	MOV rbx, 0xAAAAAAAAAAAAAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginReadingRandom64:

	MUL r9
	ADD rax, r10
	AND rax, rcx
	
	MOV rbx, qword ptr [rdi + (rax * 8)]

	DEC rsi
	JNZ beginReadingRandom64
	
	ret
	
											// ramWriteRandom32
ramWriteRandom32:

	MOV ebx, 0xAAAAAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginWritingRandom32:

	
	MUL r9
	ADD rax, r10
	AND rax, rcx 
	
	MOV dword ptr [rdi + (rax*4)], ebx

	DEC rsi
	JNZ beginWritingRandom32
	
	ret
	
											// ramReadRandom32
ramReadRandom32:

	MOV ebx, 0xAAAAAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginReadingRandom32:

	MUL r9
	ADD rax, r10
	AND rax, rcx
	
	MOV ebx, dword ptr [rdi + (rax*4)]

	DEC rsi
	JNZ beginReadingRandom32
	
	ret
		
											// ramWriteRandom16
ramWriteRandom16:

	MOV bx, 0xAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginWritingRandom16:

	
	MUL r9
	ADD rax, r10
	AND rax, rcx 
	
	MOV word ptr [rdi + (rax*2)], bx

	DEC rsi
	JNZ beginWritingRandom16
	
	ret
	
											// ramReadRandom16
ramReadRandom16:

	MOV bx, 0xAAAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginReadingRandom16:

	MUL r9
	ADD rax, r10
	AND rax, rcx
	
	MOV bx, word ptr [rdi + (rax*2)]

	DEC rsi
	JNZ beginReadingRandom16
	
	ret
	
		
											// ramWriteRandom8
ramWriteRandom8:

	MOV bl, 0xAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginWritingRandom8:

	
	MUL r9
	ADD rax, r10
	AND rax, rcx 
	
	MOV byte ptr [rdi + (rax)], bl

	DEC rsi
	JNZ beginWritingRandom8
	
	ret
	
											// ramReadRandom8
ramReadRandom8:

	MOV bl, 0xAA
	MOV rax, 1
	MOV r9, 1103515245
	MOV r10, 12345
	MOV rcx, 0x000000000FFFFFF8

	// z = (a * z + b) & m
	// m = rcx
	// a = r9
	// b = r10
	// z = rax
	
beginReadingRandom8:

	MUL r9
	ADD rax, r10
	AND rax, rcx
	
	MOV bl, byte ptr [rdi + (rax)]

	DEC rsi
	JNZ beginReadingRandom8
	
	ret
