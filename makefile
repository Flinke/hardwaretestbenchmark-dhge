
CC=gcc 
CFLAGS=-Wall -pedantic -O3 -m64 -I. -L. -L./src/
CFLAGS_RAM_TEST=-Wall -pedantic -O0 -m64 -I. -L. -L./src/
LFLAGS=--static -I. -I./src/ -L. -L./src/

all : hardwareBenchmark ramTest blockDeviceTest cpuTest

hardwareBenchmark : ./src/hardwareBenchmark.o
	$(CC) $(LFLAGS) -o $@ $^

./src/hardwareBenchmark.o : ./src/hardwareBenchmark.c
	$(CC) $(CFLAGS) -c $^ -o $@

ramTest : ./src/ramTest.o ./src/ramTestAsm.o
	$(CC) $(LFLAGS) -o $@ $^

./src/ramTest.o : ./src/ramTest.c
	$(CC) $(CFLAGS_RAM_TEST) -c $^ -o $@

./src/ramTestAsm.o : ./src/ramTest.s
	$(CC) $(CFLAGS_RAM_TEST) -c $^ -o $@

blockDeviceTest : ./src/blockDeviceTest.o
	$(CC) $(LFLAGS) -o $@ $^

./src/blockDeviceTest.o : ./src/blockDeviceTest.c
	$(CC) $(CFLAGS) -c $^ -o $@

cpuTest : ./src/cpuTest.o
	$(CC) $(LFLAGS) -o $@ $^

./src/cpuTest.o : ./src/cpuTest.c
	$(CC) $(CFLAGS) -c $^ -o $@

clean :
	rm -vrf *.o ./src/*.o  hardwareBenchmark ramTest blockDeviceTest cpuTest

# old
#allLocal: hardwaretestBenchmark ramTest blockDeviceTest cpuTest
#	
#hardwaretestBenchmark: ./src/hardwareBenchmark.c
#	gcc -Wall -O3 --static -o hardwareBenchmark ./src/hardwareBenchmark.c
#
#ramTest: ./src/ramTest.c ./src/ramTest.s
#	gcc -Wall --static -o ramTest ./src/ramTest.c ./src/ramTest.s
#
#blockDeviceTest: ./src/blockDeviceTest.c
#	gcc -Wall -O3 --static -o blockDeviceTest ./src/blockDeviceTest.c
#	
#cpuTest: ./src/cpuTest.c
#	gcc -Wall -O3 --static -o cpuTest ./src/cpuTest.c
#
# quick
allGlobal: ./src/hardwareBenchmark.c ./src/ramTest.c ./src/ramTest.s ./src/blockDeviceTest.c ./src/cpuTest.c
	gcc -Wall -O3 --static -o /usr/bin/hardwareBenchmark ./src/hardwareBenchmark.c
	gcc -Wall --static -o /usr/bin/ramTest ./src/ramTest.c ./src/ramTest.s
	gcc -Wall -O3 --static -o /usr/sbin/blockDeviceTest ./src/blockDeviceTest.c
	gcc -Wall -O3 -static -o /usr/bin/cpuTest ./src/cpuTest.c
