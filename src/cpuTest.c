#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#define IMAGE_WIDTH 2e5
#define IMAGE_HEIGHT 1e5


double calcTimeNeed(struct timespec timeEnd, struct timespec timeBegin)
{
	double timeSec = (timeEnd.tv_sec - timeBegin.tv_sec) * 1e9;
	timeSec = (timeEnd.tv_nsec + timeSec) - timeBegin.tv_nsec;
	timeSec /= 1e9; // ns to s
	
	return timeSec;
}

void primeNumbersTest()
{
	struct timespec timeBegin, timeEnd;
	unsigned long long i, maxNumberToTest;
	unsigned long long* primeNumbers = calloc(100000000, sizeof(long long));
	unsigned long primeNumbersCount = 1, k = 0;
	
	if (primeNumbers == NULL) {
		fprintf(stderr, "cpuTest : calloc error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	primeNumbers[0] = 2;
	primeNumbers[1] = 3;
	
	printf("Begin Prime Number testing\n");

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	for(i=4; i < 1000000; ++i)
	{
		if((i & 0x01) == 0x01)
		{
			maxNumberToTest = i>>1;
			for(k=1; k <= primeNumbersCount && primeNumbers[k] < maxNumberToTest; ++k)
			{
				if((i % primeNumbers[k]) == 0)
				{
					goto noPrim;
				}
			}
			primeNumbers[++primeNumbersCount] = i;
			noPrim:;
		}
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	++primeNumbersCount;
	
	
	double timeSec = calcTimeNeed(timeEnd, timeBegin);

	printf("Prime numbers calculated: %li\n", primeNumbersCount);
	printf("highest prime: %lli\n", primeNumbers[(primeNumbersCount-1)]);
	printf("Time to calculate prime numbers: %f s\n", timeSec);
	printf("Prime numbers per sec: %.2f\n", ( (double)primeNumbersCount / timeSec));
	
	free(primeNumbers);
	
	return;
}

void fractalsTest()
{
	struct timespec timeBegin, timeEnd;
	double minRe = -2.0;
	double maxRe = 1.0;
	double minIm = -1.2;
	double maxIm = minIm + (maxRe - minRe) * IMAGE_HEIGHT / IMAGE_WIDTH;
	double reFaktor = (maxRe - minRe) / (IMAGE_WIDTH-1);
	double imFaktor = (maxIm - minIm) / (IMAGE_HEIGHT-1);
	double cIm, cRe, zIm, zRe, z2Re, z2Im;
	const unsigned long maxIter = 100 ;
	unsigned long x,y,i;
	
	printf("\nBegin Fractal Test\n");
	printf("calculating frame width x height: %li x %li\n", (unsigned long)IMAGE_WIDTH, (unsigned long)IMAGE_HEIGHT);
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	for(y=0; y < IMAGE_HEIGHT; ++y)
	{
		for(x=0; x < IMAGE_WIDTH; ++x)
		{
			cRe = minRe + x * reFaktor;
			cIm = maxIm - y * imFaktor;
			zRe = cRe;
			zIm = cIm;
			for(i=0; i < maxIter; ++i)
			{
				z2Re = zRe * zRe;
				z2Im = zIm * zIm;
				if(z2Re + z2Im > 4)
				{
					// out of set
					break;
				}
			zIm = 2 * zRe * zIm + cIm;
			zRe = z2Re - z2Im + cRe;
			}
			// print
		}
	}
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	unsigned long long pixels = IMAGE_WIDTH * IMAGE_HEIGHT;
	double timeSec = calcTimeNeed(timeEnd, timeBegin);
	
	printf("calculated Pixels: %lli\n", pixels);
	printf("Time to calculate frame: %f s\n", timeSec);
	printf("Mega Pixels per sec: %.3f\n", ((double)pixels / timeSec) / 1e6);
}


int main(int argc, char **argv)
{
	
	primeNumbersTest();
	
	fractalsTest();
	
	return EXIT_SUCCESS;
}
