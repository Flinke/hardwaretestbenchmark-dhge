#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>

// 2^31 / 8 == 2^28
#define TEST_DATA_LENGTH 268435456

// 1 MiB
//#define TEST_DATA_LENGTH 13107200

#define TEST_DATA_TYPE long long

extern void ramTestSequentialDryRun(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteSequential64(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadSequ64(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteSequential32(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadSequ32(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteSequential16(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadSequ16(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteSequential8(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadSequ8(TEST_DATA_TYPE* testData, long testDataCount);

extern void ramDryRunRandom(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteRandom64(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadRandom64(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteRandom32(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadRandom32(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteRandom16(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadRandom16(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramWriteRandom8(TEST_DATA_TYPE* testData, long testDataCount);
extern void ramReadRandom8(TEST_DATA_TYPE* testData, long testDataCount);

long dryRunSeqTimeNanoSec(TEST_DATA_TYPE* testData, long testDataCount)
{
	struct timespec timeBeginDryRun, timeEndDryRun;
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBeginDryRun);
	ramTestSequentialDryRun(testData, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEndDryRun);

	timeEndDryRun.tv_sec = timeEndDryRun.tv_sec - timeBeginDryRun.tv_sec;
	
	return (timeEndDryRun.tv_nsec + (timeEndDryRun.tv_sec * 1e9)) - timeBeginDryRun.tv_nsec;
}

long dryRunRandTimeNanoSec(TEST_DATA_TYPE* testData, long testDataCount)
{
	struct timespec timeBeginDryRun, timeEndDryRun;
	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBeginDryRun);
	ramDryRunRandom(testData, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEndDryRun);

	timeEndDryRun.tv_sec = timeEndDryRun.tv_sec - timeBeginDryRun.tv_sec;
	
	return (timeEndDryRun.tv_nsec + (timeEndDryRun.tv_sec * 1e9)) - timeBeginDryRun.tv_nsec;
}

long double calculateDataPerSec(long double ramAlloc, struct timespec timeEnd, struct timespec timeBegin, long dryRunTimeNsec)
{
	long secToNsec = (timeEnd.tv_sec - timeBegin.tv_sec) * 1e9;
	long double timeSec = (timeEnd.tv_nsec + secToNsec) - timeBegin.tv_nsec - dryRunTimeNsec;
	//printf("%Lf\n", (timeSec));
	timeSec /= 1e9; // ns to s
	
	return ramAlloc / timeSec;
}


int main(int argc, char **argv)
{
	struct timespec timeBegin, timeEnd;
	
	TEST_DATA_TYPE* ptrCalloc;
	const unsigned long testDataLength = TEST_DATA_LENGTH;
	unsigned long testDataCount;
	
	unsigned long dryRunTimeNsec;
	long double mibPerSec;
	
	long double mibRamAlloc = (TEST_DATA_LENGTH * sizeof(TEST_DATA_TYPE));
	mibRamAlloc /= (1048576); // Byte to MiB
	
	printf("\nRAM Test\n");
	printf("--------\n");
	printf("\nallocating: %.1Lf MiB of memory\n\n", mibRamAlloc);
	
	
	ptrCalloc = calloc(TEST_DATA_LENGTH, sizeof(TEST_DATA_TYPE));
	//ptrCalloc = malloc(TEST_DATA_LENGTH * sizeof(TEST_DATA_TYPE));
	if (ptrCalloc == NULL){
		 fprintf(stderr, "%s: calloc error: %s\n", argv[0], strerror(errno));
		 exit(1);
	}
	
	long pointr = (long)ptrCalloc;
	printf("ptr address %p\n", (void*)ptrCalloc);
	printf("ptr to address divisible to 8: %s\n", (pointr % 8) ? "no" : "yes");
	printf("\n-------TEST TYPE-------|----TEST RESULT----\n");

	// ####### Warm up
	testDataCount = testDataLength;
	ramWriteSequential64(ptrCalloc, testDataCount);
	
	// ####### Memset
	testDataCount = testDataLength*8;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	memset(ptrCalloc, 'a' , testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);

	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, 0);
	printf("memset                 : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Dry Run Seq 64
	testDataCount = testDataLength;
	dryRunTimeNsec = dryRunSeqTimeNanoSec(ptrCalloc, testDataCount);

	// ####### Writing 64bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteSequential64(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);

	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	//printf("Dry Run Time sequential: %ld ns\n", dryRunTimeNsec);
	printf("Write seq    64bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Reading 64bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadSequ64(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);

	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  seq    64bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Dry Run Seq 32
	testDataCount = testDataLength*2;
	dryRunTimeNsec = dryRunSeqTimeNanoSec(ptrCalloc, testDataCount);
	
	// ######### Writing 32bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteSequential32(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write seq    32bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ######### Reading 32bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadSequ32(ptrCalloc, testDataCount);	
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  seq    32bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Dry Run Seq 16
	testDataCount = testDataLength*4;
	dryRunTimeNsec = dryRunSeqTimeNanoSec(ptrCalloc, testDataCount);
	
	// ######### Writing 16bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteSequential16(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write seq    16bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ######### Reading 16bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadSequ16(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  seq    16bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Dry Run Seq 8
	testDataCount = testDataLength*8;
	dryRunTimeNsec = dryRunSeqTimeNanoSec(ptrCalloc, testDataCount);
	
	// ######### Writing 8bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteSequential8(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write seq    8bit      : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ######### Reading 8bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadSequ8(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  seq    8bit      : %10.3Lf MiB/s\n", (mibPerSec));

	
	// ###### Dry Run Random 64
	testDataCount = testDataLength;
	dryRunTimeNsec = dryRunRandTimeNanoSec(ptrCalloc, testDataCount);
	//printf("Dry Run Time random    : %ld ns\n", dryRunTimeNsec);
	
	// ####### Ram write Random 64bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteRandom64(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write random 64bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Ram read Random 64bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadRandom64(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  random 64bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ###### Dry Run Random 32
	testDataCount = testDataLength*2;
	dryRunTimeNsec = dryRunRandTimeNanoSec(ptrCalloc, testDataCount);
	//printf("Dry Run Time random    : %ld ns\n", dryRunTimeNsec);
	
	// ####### Ram write Random 32bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteRandom32(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write random 32bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Ram read Random 32bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadRandom64(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  random 32bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ###### Dry Run Random 16
	testDataCount = testDataLength*4;
	dryRunTimeNsec = dryRunRandTimeNanoSec(ptrCalloc, testDataCount);
	//printf("Dry Run Time random    : %ld ns\n", dryRunTimeNsec);
	
	// ####### Ram write Random 32bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteRandom16(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write random 16bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Ram read Random 32bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadRandom16(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  random 16bit     : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ###### Dry Run Random 8
	testDataCount = testDataLength*8;
	dryRunTimeNsec = dryRunRandTimeNanoSec(ptrCalloc, testDataCount);
	
	// ####### Ram write Random 8bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramWriteRandom8(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Write random 8bit      : %10.3Lf MiB/s\n", (mibPerSec));
	
	// ####### Ram read Random 8bit
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeBegin);
	ramReadRandom8(ptrCalloc, testDataCount);
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &timeEnd);
	
	mibPerSec = calculateDataPerSec(mibRamAlloc, timeEnd, timeBegin, dryRunTimeNsec);
	printf("Read  random 8bit      : %10.3Lf MiB/s\n", (mibPerSec));
	
	// free RAM
	free(ptrCalloc);
	
	return 0;
}
